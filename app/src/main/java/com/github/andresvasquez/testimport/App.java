package com.github.andresvasquez.testimport;

import android.content.Context;

import io.quisque.sdk.QuisqueApp;
import io.quisque.sdk.model.QuisqueSDKMapOptions;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by andresvasquez on 4/23/18.
 */

public class App extends QuisqueApp {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Set default font for app
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/MyCustomFont.ttf")
                .setFontAttrId(io.quisque.sdk.R.attr.fontPath)
                .build());

        //Init Quisque SDK with config variables
        QuisqueSDKBuilder builder = new QuisqueSDKBuilder();
        builder.setApplicationID(ApiConstants.APPLICATION_ID);
        builder.setServerUrl(ApiConstants.URL);
        builder.setEndpointApi(ApiConstants.ENDPOINT_API);
        builder.setGoogleApiKey("AIzaSyBUkykEjQQ-iWoTR9EW0yBIk6WijBtErj");
        builder.setEndpointVersion(ApiConstants.ENDPOINT_VERSION);
        builder.init();

        // Init Quisque SDK Map
        QuisqueSDKMapOptions mapOptions = new QuisqueSDKMapOptions();
        mapOptions.setMarkerEnabled(R.drawable.marker_enabled_normal);
        mapOptions.setMarkerEnabledSelected(R.drawable.marker_enabled_selected);
        mapOptions.setMarkerDisabled(R.drawable.marker_disabled);
        mapOptions.setMarkerDisabledSelected(R.drawable.marker_disabled);
        mapOptions.setMarkerActive(R.drawable.marker_selected);
        mapOptions.setMarkerNotAvailable(R.drawable.marker_coming_soon);
        mapOptions.setMarkerNotFoundLocation(R.drawable.icon_marker_search_location);
        mapOptions.setMarkerMyLocation(R.drawable.marker_blue);
        mapOptions.setMarkerClusterDisabled(R.drawable.marker_cluster_disabled);
        mapOptions.setMarkerClusterEnabled(R.drawable.marker_cluster_enabled);
        mapOptions.setMarkerClusterEnabledBig(R.drawable.marker_cluster_enabled_big);
        mapOptions.setHamburgerButtonEnabled(true);

        // Set map options for the QuisqueSDK builder
        builder.setMapOptions(mapOptions);

        builder.init();
    }
}
