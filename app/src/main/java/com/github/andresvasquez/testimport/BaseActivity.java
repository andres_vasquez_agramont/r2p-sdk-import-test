package com.github.andresvasquez.testimport;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import io.quisque.sdk.sdk.helpers.QuisqueSDKHelper;
import io.quisque.sdk.sdk.helpers.QuisqueSDKHelperImpl;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by andresvasquez on 4/23/18.
 */

public class BaseActivity extends AppCompatActivity {
    QuisqueSDKHelper quisqueSDKHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        quisqueSDKHelper = QuisqueSDKHelperImpl.getInstance(this, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        quisqueSDKHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        quisqueSDKHelper = QuisqueSDKHelperImpl.getInstance(this, this);
    }

    // Optional if you are using Calligraphy
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
