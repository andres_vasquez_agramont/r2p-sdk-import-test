package com.github.andresvasquez.testimport;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import io.quisque.sdk.ui.fragments.base.MapBaseFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MapBaseFragment fragment = new MapBaseFragment();

        getSupportFragmentManager().beginTransaction()
                .add(R.id.containerFrameLayout, fragment, fragment.getClass().getName())
                .commitAllowingStateLoss();
    }
}
