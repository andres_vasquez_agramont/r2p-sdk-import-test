package com.github.andresvasquez.testimport;

/**
 * Created by andresvasquez on 10/18/17.
 */

public class ApiConstants {
    public static final String APPLICATION_ID = "ready2paorutbxhw";
    public static final String URL = "http://dev.quisque.io:8080";
    public static final String ENDPOINT_API = "api";
    public static final String ENDPOINT_VERSION = "v1";
}